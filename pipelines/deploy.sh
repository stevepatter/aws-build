#!/usr/bin/env sh
set -e
# Export image name
export IMAGE_NAME=755399320166.dkr.ecr.eu-west-1.amazonaws.com/intouchnetworks/${DOCKER_APP_NAME}:${BITBUCKET_BRANCH}-${BITBUCKET_COMMIT}
# ECR login
eval $(aws ecr get-login --region ${AWS_DEFAULT_REGION} --no-include-email)
# Update ECS Service
export CLUSTER_NAME=$(echo "ITN-${BITBUCKET_BRANCH}" | awk '{print toupper($0)}' | sed 's/-MASTER//')
echo "Cluster:" $CLUSTER_NAME
export NGINX_SERVER_NAME=$([ "$BITBUCKET_BRANCH" = "master" ] && echo "$DOCKER_APP_URL" || echo "$BITBUCKET_BRANCH.$DOCKER_APP_URL")
echo "Nginx Server Name:" $NGINX_SERVER_NAME
export DOCKER_ENV=$([ "$BITBUCKET_BRANCH" = "master" ] && echo "production" || echo "$BITBUCKET_BRANCH")
echo "Docker Env:" $DOCKER_ENV
/bin/bash -c "envsubst < .docker/.container-definition/${ECS_TEMPLATE}.template.json > .docker/.container-definition/${DOCKER_APP_NAME}.json"
export TASK_VERSION=$(aws ecs register-task-definition --cli-input-json file://${BITBUCKET_CLONE_DIR}/.docker/.container-definition/${DOCKER_APP_NAME}.json --family ${DOCKER_APP_NAME}-${BITBUCKET_BRANCH} | jq --raw-output '.taskDefinition.revision')
echo "ECS Task Definition:" $TASK_VERSION 
export SERVICE_NAME=$([ "$BITBUCKET_BRANCH" = "master" ] && echo "$DOCKER_APP_NAME" || echo "$DOCKER_APP_NAME-$BITBUCKET_BRANCH")
echo "ECS Service Name:" $SERVICE_NAME
aws ecs update-service --cluster ${CLUSTER_NAME} --service ${SERVICE_NAME} --task-definition ${DOCKER_APP_NAME}-${BITBUCKET_BRANCH}:$TASK_VERSION
