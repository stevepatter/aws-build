#!/usr/bin/env python3

"""
Switch the cloudfront path used on a distribution

usage: switch-path.py --distribution [distribution id] --path [path]
"""

import boto3
import argparse
import json
import pprint
import sys

def dd(data):
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(data)
    sys.exit()

"""
Get initial arguments
"""
parser = argparse.ArgumentParser()
parser.add_argument('--distribution', required=True, dest='distribution', help='Cloudfront distribution ID, e.g. E10FHP6J4J3PKL')
parser.add_argument('--path', required=True, dest='path', help='Directory within the base bucket')
args = parser.parse_args()

"""
Get current distribution config
"""
cloudfront = boto3.client('cloudfront')

res = cloudfront.get_distribution_config(Id = args.distribution)
etag = res['ETag']
distributionConfig = res['DistributionConfig']

"""
Set path on Origin
"""
distributionConfig['Origins']['Items'][0]['OriginPath'] = '/' + args.path
distributionConfig['CustomErrorResponses']['Items'][0]['ResponsePagePath'] = '/' + args.path + '/index.html'
#dd(distributionConfig)

"""
Set default 404 redirect on Origin
"""
distributionConfig['CustomErrorResponses'] = {
    'Quantity' : 1,
    'Items': [
        {
            'ErrorCode' : 404,
            'ResponsePagePath': '/index.html',
            'ResponseCode': '200',
            'ErrorCachingMinTTL': 300
        }
    ]
}

"""
Upload modified configuration to CloudFront
"""
cloudfront.update_distribution(
    DistributionConfig = distributionConfig,
    Id = args.distribution,
    IfMatch = etag
)

"""
Set permissions on S3 to match - may not need this
( / loads, /login gives permission errors )
"""
