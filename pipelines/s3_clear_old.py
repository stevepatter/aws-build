#!/usr/bin/env python3

"""
Delete old portal code versions from S3

Directories don't have a creation time. However the deployment strategy
prefixes each build directory with an incrementing ID. use this to decide which
directories to keep
"""

import boto3
import argparse
import json
import pprint
import sys
import re

def dd(data):
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(data)
    sys.exit()

"""
Get initial arguments
"""
parser = argparse.ArgumentParser()
parser.add_argument('--bucket', required=True, dest='bucket', help='S3 Bucket Name')
parser.add_argument('--revision', required=True, dest='revision', help='Latest revision deployed')
args = parser.parse_args()

# connect to s3 & list folders in bucket
client = boto3.client('s3')
result = client.list_objects_v2(
    Bucket = args.bucket,
    Delimiter = '/'
)

# deletion function
def deltree(client, bucket, path):
    filesToDelete = []
    for i in (client.list_objects_v2(
        Bucket = bucket,
        Prefix = path
    )['Contents']):
        filesToDelete.append({ 'Key': i['Key']})
    
    client.delete_objects(
        Bucket = bucket,
        Delete = { 'Objects' : filesToDelete }
    )
    print("deleted: %s" % path)

# find folders we need to delete 
# must start numeric and be sufficiently below the current rev number
for o in result.get('CommonPrefixes'):
    folder = o.get('Prefix')
    if (re.search('^\d+-', folder)):
        pathparts = folder.split('-')
        if (int(pathparts[0]) < int(args.revision) - 50 ): # how many revisions to keep
            deltree( client, args.bucket, folder )

