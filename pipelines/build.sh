#!/usr/bin/env sh
set -e

custom_buildfile='.deploy/build.sh'
if [ -f ${custom_buildfile} ]; then
   echo "Custom app build"
   . ${custom_buildfile}
else
   echo "Standard app build"
   # Git LFS pull (pop to cache)
   git lfs pull
   
   # Build
   if [ -f 'composer.json' ]; then
      composer install --optimize-autoloader --no-dev --prefer-source --ignore-platform-reqs
   fi
   if [ -f 'artisan' ]; then
      cp .env.example .env
      php artisan key:generate
   fi
   if [ -f 'package.json' ]; then
      yarn install
   fi
   if [ -f 'gulpfile.js' ]; then
      gulp build
   fi
fi

