#!/usr/bin/env sh

### Image build
eval $(aws ecr get-login --region ${AWS_DEFAULT_REGION} --no-include-email)

# Build image
custom_dockerfile='.deploy/docker/Dockerfile'
if [ -f ${custom_dockerfile} ]; then
   dockerfile=${custom_dockerfile}
else
   dockerfile="./.docker/${ECS_TEMPLATE}/Dockerfile"
fi
export IMAGE_NAME=644381514895.dkr.ecr.eu-central-1.amazonaws.com/sites/${DOCKER_APP_NAME}:${IMAGE_TAG}
docker build --build-arg DOCKER_APP_NAME=${DOCKER_APP_NAME} --file ${dockerfile} -t $IMAGE_NAME .
echo $IMAGE_NAME
docker push $IMAGE_NAME

# publish with cloudformation
${CI_PROJECT_DIR}/.docker/cloudformation/publish-cloudformation.py `echo $IMAGE_NAME`
