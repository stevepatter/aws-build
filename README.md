# Docker build & deploy scripts

## Build

These are used for any builds to AWS Fargate. Typically your project pipeline will have a build section containing
```yaml
script:
  - git clone --branch master --depth 1 git@bitbucket.org:intouchnetworks/docker.git .docker
  - export DOCKER_APP_NAME="your_app_name"
  - export AWS_DEFAULT_REGION="eu-west-1"
  - export ECS_TEMPLATE=lep
  - /bin/bash .docker/.pipelines/build.sh
```

The `pipelines/build.sh` script here contains default commands to build & install most node or php/composer based applications
into a Docker image, and can be overridden. The `ECS_TEMPLATE` environment variable specifies which docker build file 
(from a directory the same name as the `ECS_TEMPLATE` variable) is used.

There's a couple of basic options available, which can be used together or you can use whichever is appropriate 

1) if you need a custom application build from the usual `npm install && gulp build` style installation, create 
`.deploy/build.sh` with any commands as required.
2) if instead you want a custom Docker image, create `.deploy/docker/Dockerfile` with appropriate commands. This is 
typically used if you need some additional software installed to run the application (such as extra PHP modules) or
if you need a different supervisord setup to start necessary processes. Our regular docker images (under the `build` 
hierarchy on ECR) contain either enough to run a node service, or a Laravel PHP application.

## Deploy via Cloudformation

We can have a number of Cloudformation templates for defining services, each of these can take several parameters (such as 
VPC ID, Loadbalancer ARN etc) from a pair of config files named after the branch being deployed. The templates are
stored in an S3 bucket.

Each template defines defaults for some parameters, these are then overridden by a site configuration file at 
`.deploy/site.branchname.yml` in your own project, typically 
to set the service name & other parameters specific to an environment.

#### Developing with this

A copy of the Cloudformation templates is stored in `cloudformation/base-templates` for reference. These templates
are not used from here and must be uploaded to S3 if they are changed. I've provided a script `upload-base-template.sh`
in the templates directory, this will upload to the right place.

#### Sample yml configuration for your project

This is for a service using the node template, this has different environment variables & load balancer configuration
from the other generic `fargate.yml` template.

```yaml
# configuration specific to the dev instance
# there will be a similar file for live (master) with different config
Template: 'https://s3-eu-west-1.amazonaws.com/cloudformation-templates/node.yml'
Parameters:
  StackName: 'api-forums-dev'
  ClusterName: 'API-DEV'
```
etc, filling in parameters in the base template.

#### Finding a spare load order priority

On ALBs, the rule priority number is not as displayed on AWS web console (though they are in the same order),
so you need to list the actual priority (as below) to find a spare one.

`aws elbv2 describe-rules --listener-arn='${ARN}'|grep Priority`
