#!/usr/bin/python

"""
Publish new service or new version of service via Cloudformation

see README.md for more information
"""

import json
import sys
import os
import re
from yaml import load, dump

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
from subprocess import call, check_output

"""
Generate merged configuration
"""


def mergeConfig():
    siteConfigFile = os.environ['CI_PROJECT_DIR'] + '/.deploy/site.' + os.environ['CI_BUILD_REF_NAME'] + '.yml'

    # initialise with empty parameters
    config = {'Parameters': {}}

    siteConfig = load(open(siteConfigFile), Loader=Loader)

    for key, value in siteConfig['Parameters'].items():
        if key in os.environ:
            config['Parameters'][key] = os.environ[key]
        else:
            config['Parameters'][key] = value

    for key, value in siteConfig.items():
        if key != 'Parameters':
            config[key] = value

    return config


"""
Translate our simple config into CloudFront parameter style
"""


def generateStackConfig(config):
    cloudformationConfig = []
    for key, value in config['Parameters'].items():
        cloudformationConfig.append({
            'ParameterKey': key,
            'ParameterValue': str(value),
        })

    # push branch name to cloudfront - often used to assemble fargate task name or log names
    #cloudformationConfig.append({
    #    'ParameterKey': 'BitbucketBranch',
    #    'ParameterValue': os.environ['CI_BUILD_REF_NAME'],
    #})

    cloudformationConfig.append({
        'ParameterKey': 'ImageUrl',
        'ParameterValue': sys.argv[1]
    })
    print(json.dumps(cloudformationConfig, indent=4))

    return json.dumps(cloudformationConfig, indent=4)


"""
Create new CloudFormation stack to run our service/config
"""


def createStack(config):
    print("Creating Stack")
    # createLogGroup(config)
    pushStack('create-stack', config)


"""
Update current cloudformation stack
"""


def updateStack(config):
    print("Updating Stack")
    pushStack('update-stack', config)


"""
Publish stack on cloudformation
"""


def pushStack(command, config):
    print(check_output([
        'aws', 'cloudformation', command,
        '--stack-name', config['StackName'],
        '--template-url', config['Template'],
        '--parameters', generateStackConfig(config)
    ]))


"""
Check whether the stack exists
If it does, result of describe-stacks will be json.
"""


def stackExists(stackName):
    res = call(['aws', 'cloudformation', 'describe-stacks', '--stack-name', stackName])
    if (res == 0):
        return True
    else:
        return False


"""
Create log group if not present
describe-log-groups always returns an array of logGroups (json), check length
Only make log group if we're pushing to ECS (grodly, I know but)
"""


def createLogGroup(config):
    if (sys.argv[1] == None):
        return False

    logGroupName = '/ecs/' + config['Parameters']['StackName']

    res = check_output(['aws', 'logs', 'describe-log-groups', '--log-group-name-prefix', logGroupName])
    data = json.loads(res.decode())
    if (len(data['logGroups']) == 0):
        call(['aws', 'logs', 'create-log-group', '--log-group-name', logGroupName])


"""
Main

If stack does not exist, create stack
Else update stack
"""
config = mergeConfig()
# createLogGroup(config)
data = stackExists(config['StackName'])
print("checking for stack %s" % config['StackName'])
if (data):
    print("Stack Exists, updating")
    updateStack(config)
else:
    print("stack does not exist, creating")
    createStack(config)
